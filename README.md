# Enzo2 Prompt for Zsh
This is a prompt theme for zsh, inspired by adam2.

## Description
Like adam2, the working dir and user/hostname are printed a line above the command prompt itself.

Unlike adam2:
- Unicode box-drawing characters are used if possible for a sleeker look
- Tasteful half-dash box-drawing characters are used instead of parentheses around text
- Full-width and zero-width Unicode characters are handled correctly
- The user/host is colored red when logged-in as root

Many of these features were pioneered by [prompt_kir](https://github.com/kirelagin/prompt_kir). However, prompt_kir runs as a generic zsh script, so it does not work with the `prompt` command as expected.

The prompt also works properly in the Linux framebuffer terminal (TTY), with some compromises. Make sure to use a font with the appropriate code-page 437 box-drawing characters.

Also, the color scheme is specifically suited to the Solarized color schemes. The standard Solarized palette has grey/neutral tones where some of the "bright" colors typically are, so that "bright green" comes out dark grey. It should look OK with normal color schemes too.

## Visuals
![Prompt in the home dir](/images/home.png)
![Prompt with a short dir path](/images/smallpath.png)
![Prompt with a long dir path, which overtakes the user/hostname](/images/longpath.png)
![Prompt with an even longer path, truncated to fit on one line](/images/longerpath.png)
![Prompt with fullwidth characters in the dir name](/images/fullwidth-chars.png)
![Prompt running as root](/images/root.png)

## Installation
Copy the prompt file into your zsh site-functions folder as root:
```
# cp prompt_enzo2_setup /usr/local/share/zsh/site-functions/
```
Create this directory path if it does not exist already.

Try it out:
```
$ prompt enzo2
```

To make it the default, add to your `~/.zshrc`:
```
autoload -U promptinit
promptinit
prompt enzo2
```

## Acknowledgments
The zsh syntax can be difficult; I could not have done this without these great examples.

[Adam Spiers](https://adamspiers.org/computing/zsh/)

[kirelagin](https://github.com/kirelagin/prompt_kir)

[Phil!](http://aperiodic.net/phil/prompt/)

## License
MIT License
